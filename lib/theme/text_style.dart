import 'package:flutter/material.dart';
import 'package:fipex_app/theme/color.dart';

class TextStyles {
  final BuildContext context;

  TextStyles(this.context);

  get get => null;

  TextStyle getTitleStyle() {
    return Theme.of(context).textTheme.headline6!.copyWith(
          fontSize: 24,
          fontWeight: FontWeight.w700,
          color: mainColor,
        );
  }

  TextStyle getDescriptionStyle() {
    return Theme.of(context).textTheme.bodyText1!.copyWith(
          color: labelColor,
        );
  }

  TextStyle getRegularStyle() {
    return Theme.of(context).textTheme.bodyText1!.copyWith(
          fontSize: 16,
          color: textColor,
        );
  }

  TextStyle getBoldStyle() {
    return Theme.of(context)
        .textTheme
        .subtitle1!
        .copyWith(fontSize: 20, color: textColor, fontWeight: FontWeight.w700);
  }
}

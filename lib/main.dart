import 'package:camera/camera.dart';
import 'package:fipex_app/API/Model.dart';
import 'package:fipex_app/API/Repository.dart';
import 'package:fipex_app/components/qr_scanner.dart';
import 'package:fipex_app/screens/auth/Login.dart';
import 'package:fipex_app/screens/auth/Registration.dart';
import 'package:fipex_app/screens/home.dart';
import 'package:flutter/material.dart';
import 'package:fipex_app/screens/DetailProduct.dart';
import 'package:fipex_app/screens/auth/Landing.dart';
import 'package:fipex_app/screens/products.dart';
import 'package:fipex_app/screens/setting.dart';
import 'package:fipex_app/widgets/TakePictureScreen.dart';
import 'screens/root_app.dart';
import 'theme/color.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

final ValueNotifier selectedIndexGlobal = ValueNotifier(0);

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Fipex',
      theme: ThemeData(
        primaryColor: primary,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => RootApp(),

        '/qrscanner': (context) => QRScanner(),

        //authentication
        '/auth/landing': (context) => LandingPage(),
        '/auth/login': (context) => LoginPage(),
        '/auth/register': (context) => RegistrationPage(),

        //home
        '/products': (context) => ProductsPage(),
        '/detailproduct': (context) => DetailProduct(),
        '/setting': (context) => SettingPage(),
      },
    );
  }

  getTakePictureScreen() async {
    final cameras = await availableCameras();
    final firstCamera = cameras.first;
    return TakePictureScreen(camera: firstCamera);
  }
}

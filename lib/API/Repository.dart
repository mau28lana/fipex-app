import 'dart:convert';

import 'package:fipex_app/API/Model.dart';
import 'package:fipex_app/screens/root_app.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Repository {
  final _baseUrl = "https://apis.ruang-ekspresi.id/fipex-v2/index.php";
  final _urlOrang =
      "https://brotherlike-navies.000webhostapp.com/people/people.php";

  //get methode

  Future getGuestdata() async {
    try {
      final response = await http.get(Uri.parse(_baseUrl + "/users"));
      if (response.statusCode == 200) {
        Iterable it = jsonDecode(response.body);
        List<Guest> guest = it.map((e) => Guest.fromJson(e)).toList();

        return guest;
      }
    } catch (e) {}
  }

  Future getProducts() async {
    try {
      final response = await http.get(Uri.parse(_baseUrl + "/products"));
      if (response.statusCode == 200) {
        var body = jsonDecode(response.body);
        Iterable it = body['data'];
        List<Products> products = it.map((e) => Products.fromJson(e)).toList();

        return products;
      }
    } catch (e) {}
  }

  Future getProductDetail({String? productId}) async {
    final response = await http
        .get(Uri.parse(_baseUrl + "/products/" + productId! + "/detail"));

    if (response.statusCode == 200) {
      // Parse the response body as a Product object
      return Products.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load product');
    }
  }

  Future getCategories() async {
    try {
      final response = await http.get(Uri.parse(_baseUrl + "/categories"));
      if (response.statusCode == 200) {
        var body = jsonDecode(response.body);
        Iterable it = body;

        List<Category> categories =
            it.map((e) => Category.fromJson(e)).toList();

        return categories;
      }
    } catch (e) {}
  }

  Future<String?> getToken() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString('token');
  }

  //post methode

  Future reqLogin(email, password) async {
    try {
      final response = await http.post(Uri.parse(_baseUrl + "/auth/login"),
          headers: {"Content-Type": "application/json"},
          body: jsonEncode({"email": email, "password": password}));

      // final user = await http.get(Uri.parse(_baseUrl + "/users"));
      if (response.statusCode == 200) {
        final responseJson = jsonDecode(response.body);
        final token = responseJson['token'];

        // final responseUser = jsonDecode(user.body);
        // final userId = responseUser['id'];

        saveToken(token);
        getToken();

        navigateToHomePage(context);

        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e.toString);
    }
  }

  Future registerData(name, bio, email, password) async {
    try {
      final response = await http.post(Uri.parse(_baseUrl + "/auth/register"),
          body: jsonEncode({
            "name": name,
            "bio": bio,
            "email": email,
            "password": password
          }));

      print(response.body);
      if (response.statusCode == 201) {
        print(response.statusCode.toString());
        print(response.body);
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e.toString());
    }
  }
}

void saveToken(String token) async {
  final prefs = await SharedPreferences.getInstance();
  prefs.setString('token', token);
}

void navigateToHomePage(context) {
  Navigator.of(context).pushReplacementNamed("/");
}

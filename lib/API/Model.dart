class Guest {
  final String id;
  final String email;
  final String name;
  final String? bio;
  final String? img;

  const Guest({
    required this.id,
    required this.email,
    required this.name,
    this.bio,
    this.img,
  });

  factory Guest.fromJson(Map<String, dynamic> json) {
    return Guest(
        id: json['id'],
        email: json['email'],
        name: json['name'],
        bio: json['bio'],
        img: json['img_url']);
  }
}

class Products {
  String? id;
  String? categoryId;
  String? exhibitionId;
  String? authorId;
  String? name;
  String? description;
  String? totalPoints;
  // final String? img;

  Products({
    this.id,
    this.categoryId,
    this.exhibitionId,
    this.authorId,
    this.name,
    this.description,
    this.totalPoints,
    // this.img,
  });

  factory Products.fromJson(Map<String, dynamic> json) {
    return Products(
      id: json['id'],
      categoryId: json['category_id'],
      exhibitionId: json['exhibition_id'],
      authorId: json['author_id'],
      name: json['name'],
      description: json['description'],
      totalPoints: json['total_points'],
    );
  }
}

class Category {
  String id;

  Category({required this.id});

  factory Category.fromJson(dynamic json) {
    return Category(id: json['id']);
  }
}

class Token {
  String token;

  Token({required this.token});
  factory Token.fromJson(dynamic object) {
    return Token(token: object['token']);
  }
}

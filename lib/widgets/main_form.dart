import 'package:flutter/material.dart';

class MainForm extends StatelessWidget {
  final String? text;
  final bool isPerm;
  final TextEditingController? controller;
  const MainForm({
    Key? key,
    this.text,
    this.controller,
    required this.isPerm,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: isPerm,
      controller: controller,
      decoration: InputDecoration(
          hintText: text,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0))),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "please enter some text";
        }
        return null;
      },
    );
  }
}

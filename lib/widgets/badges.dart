import 'package:flutter/material.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/theme/text_style.dart';
import 'package:fipex_app/utils/data.dart';

class Badges extends StatefulWidget {
  final Function()? onTap;
  final Function? onPressed;
  final bool isProduct;
  final String title;
  final double height;
  late final int futureBadge;
  late final int silverBadge;
  Badges({
    Key? key,
    this.onTap,
    this.title = "Lencana Produk",
    this.isProduct = true,
    this.height = 100,
    this.futureBadge = 10,
    this.silverBadge = 0,
    this.onPressed,
  }) : super(key: key);

  @override
  State<Badges> createState() => _BadgesState();
}

class _BadgesState extends State<Badges> {
  late int userBadgeInv = 10;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        padding: EdgeInsets.symmetric(vertical: 16),
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.title,
              style: TextStyles(context).getBoldStyle(),
            ),
            const SizedBox(height: 12),
            GestureDetector(
              onTap: widget.onTap,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 28),
                width: size.width,
                height: widget.height,
                decoration: BoxDecoration(
                    color: blue, borderRadius: BorderRadius.circular(8)),
                child: widget.isProduct ? badgesProduct() : badgesUser(),
              ),
            )
          ],
        ));
  }

  badgesProduct() {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.star,
            color: diamond,
            size: 50,
          ),
          Text(badgeProduct[0]["platinum"].toString())
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.star,
            color: gold,
            size: 50,
          ),
          Text(badgeProduct[0]["gold"].toString())
        ],
      ),
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.star,
            color: silver,
            size: 50,
          ),
          Text((badgeProduct[0]["silver"] + widget.silverBadge).toString())
        ],
      ),
    ]);
  }

  badgesUser() {
    return GridView.builder(
        shrinkWrap: true,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            mainAxisExtent: 70, childAspectRatio: 1, crossAxisCount: 5),
        itemCount: widget.futureBadge,
        itemBuilder: (_, index) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.star,
                color: silver,
                size: 40,
              )
            ],
          );
        });
  }
}

import 'package:flutter/material.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/widgets/favorite_box.dart';
import 'custom_image.dart';

class ProductPoster extends StatelessWidget {
  ProductPoster(
      {Key? key,
      required this.data,
      this.width = 280,
      this.height = 300,
      this.onTap,
      this.onTapFavorite})
      : super(key: key);
  final data;
  final double width;
  final double height;
  final GestureTapCallback? onTapFavorite;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        height: height,
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(bottom: 5, top: 5),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
              color: shadowColor.withOpacity(0.1),
              spreadRadius: 1,
              blurRadius: 1,
              offset: Offset(1, 1), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [getPoster()],
        ),
      ),
    );
  }

  getPoster() {
    if (data['image'] != null) {
      return CustomImage(
        data["image"],
        width: double.infinity,
        height: 170,
        radius: 15,
      );
    } else if (data['image'] == null) {
      return BlankImageWidget();
    }
  }
}

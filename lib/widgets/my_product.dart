import 'package:carousel_slider/carousel_slider.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/theme/text_style.dart';
import 'package:fipex_app/utils/data.dart';
import 'package:fipex_app/widgets/main_button.dart';
import 'package:fipex_app/widgets/product_poster.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class MyProduct extends StatelessWidget {
  const MyProduct({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 24),
          CarouselSlider(
            options: CarouselOptions(
              aspectRatio: 0.1,
              height: 200,
              enlargeCenterPage: true,
              disableCenter: true,
              viewportFraction: .75,
            ),
            items: List.generate(
              features.length,
              (index) => ProductPoster(
                data: features[index],
              ),
            ),
          ),
          const SizedBox(height: 24),
          SizedBox(
            width: size.width * 0.9,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Kreator",
                  style: TextStyles(context).getBoldStyle(),
                ),
                SingleChildScrollView(
                  padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: List.generate(
                      creator.length,
                      (index) => Padding(
                          padding: const EdgeInsets.only(right: 10, top: 20),
                          child: getCreatorItem(data: creator[index])),
                    ),
                  ),
                ),
              ],
            ),
          ),
          getDescription(context),
          const SizedBox(height: 24),
          MainButton(
            text: "Edit Deskripsi",
            onTap: () {},
          )
        ],
      ),
    );
  }

  getCreatorItem({onTap, required data}) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              width: 60,
              height: 60,
              decoration: BoxDecoration(
                  color: labelColor, borderRadius: BorderRadius.circular(100)),
              child: data["icon"] ?? Icon(Icons.add)),
          Container(
            height: 50,
            width: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Expanded(
                    child: Text(
                  data['name'] ?? "Tambahkan kreator",
                  textAlign: TextAlign.center,
                )),
              ],
            ),
          )
        ],
      ),
    );
  }

  getDescription(context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Description",
              style: TextStyles(context).getBoldStyle(),
            ),
            SizedBox(height: 8),
            Text(features[0]["description"]),
          ],
        ));
  }
}

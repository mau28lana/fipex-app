import 'package:fipex_app/theme/color.dart';
import 'package:flutter/material.dart';

class MainButton extends StatelessWidget {
  final Function()? onTap;
  final String text;
  final double? width;
  const MainButton({Key? key, this.onTap, required this.text, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: ElevatedButton(
        onPressed: onTap,
        style: ElevatedButton.styleFrom(backgroundColor: blue),
        child: SizedBox(
          height: 42,
          width: width ?? size.width * 0.7,
          child: Center(
            child: Text(
              text,
              style: TextStyle(
                  color: white, fontSize: 20, fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ),
    );
    ;
  }
}

import 'package:fipex_app/theme/text_style.dart';
import 'package:flutter/material.dart';
import 'package:fipex_app/theme/color.dart';
import 'custom_image.dart';

class ProductCard extends StatelessWidget {
  ProductCard(
      {Key? key, required this.title, this.exhibition, this.img, this.onTap})
      : super(key: key);
  final title;
  final String? img, exhibition;
  final GestureTapCallback? onTap;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onTap,
      child: Container(
          margin: const EdgeInsets.only(bottom: 10, left: 10, right: 10),
          padding: const EdgeInsets.all(10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 1,
                blurRadius: 1,
                offset: Offset(1, 1), // changes position of shadow
              ),
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // getImg(size),
              CustomImage(
                img ?? "../../assets/img/blank-img.png",
                radius: 5,
                width: size.width * 0.5,
              ),
              const SizedBox(
                height: 8.0,
              ),
              Container(
                width: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyles(context)
                          .getBoldStyle()
                          .copyWith(fontSize: 16),
                    ),
                    const SizedBox(
                      height: 12,
                    ),
                    Text(
                      exhibition!,
                      style: TextStyles(context).getDescriptionStyle(),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }

  // getImg(Size size) {
  //   if (img != null) {
  //     return CustomImage(
  //       data["image"],
  //       radius: 5,
  //       width: size.width * 0.5,
  //     );
  //   } else if (data['image'] == null) {
  //     return BlankImageWidget(
  //       height: 100,
  //     );
  //   }
  // }
}

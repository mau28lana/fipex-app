import 'package:flutter/material.dart';
import 'package:fipex_app/theme/color.dart';
import 'custom_image.dart';

class CategoryItem extends StatelessWidget {
  CategoryItem(
      {Key? key, required this.data, this.onTap, this.color = appBgColor})
      : super(key: key);
  final data;
  final GestureTapCallback? onTap;
  final Color color;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: 150,
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        child: Text(
          data["name"],
          style: TextStyle(fontSize: 14),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
          // Container(
          //     margin: EdgeInsets.only(right: 5, top: 8.0),
          //     decoration: BoxDecoration(
          //       borderRadius: BorderRadius.circular(15),
          //       boxShadow: [
          //         BoxShadow(
          //           color: Colors.grey.withOpacity(0.1),
          //           spreadRadius: 1,
          //           blurRadius: 1,
          //           offset: Offset(1, 1), // changes position of shadow
          //         ),
          //       ],
          //     ),
          //     child: CustomImage(
          //       data["image"],
          //       radius: 15,
          //       height: 50,
          //       width: 50,
          //     )),

                  // Expanded(
                  //   child: Column(
                  //     crossAxisAlignment: CrossAxisAlignment.start,
                  //     children: [
                  //       Text(
                  //         data["name"],
                  //         maxLines: 1,
                  //         overflow: TextOverflow.ellipsis,
                  //         style: TextStyle(
                  //             color: textColor,
                  //             fontSize: 16,
                  //             fontWeight: FontWeight.w600),
                  //       ),
                  //       SizedBox(
                  //         height: 5,
                  //       ),
                  //       Text(
                  //         data["type"],
                  //         style: TextStyle(fontSize: 12, color: labelColor),
                  //       ),
                  //       SizedBox(
                  //         height: 15,
                  //       ),
                  //       Row(
                  //         children: [
                  //           Icon(
                  //             Icons.star,
                  //             size: 14,
                  //             color: yellow,
                  //           ),
                  //           SizedBox(
                  //             width: 3,
                  //           ),
                  //           Expanded(
                  //             child: Text(
                  //               data["rate"],
                  //               style: TextStyle(fontSize: 12, color: Colors.grey),
                  //             ),
                  //           ),
                  //           Text(
                  //             data["price"],
                  //             style: TextStyle(
                  //               fontSize: 14,
                  //               fontWeight: FontWeight.w500,
                  //               color: primary,
                  //             ),
                  //           ),
                  //         ],
                  //       ),
                  //     ],
                  //   ),
                  // )
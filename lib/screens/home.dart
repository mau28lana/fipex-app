import 'package:carousel_slider/carousel_slider.dart';
import 'package:fipex_app/API/Model.dart';
import 'package:fipex_app/API/Repository.dart';
import 'package:fipex_app/components/qr_scanner.dart';
import 'package:fipex_app/main.dart';
import 'package:fipex_app/screens/root_app.dart';
import 'package:fipex_app/theme/text_style.dart';
import 'package:flutter/material.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/utils/data.dart';
import 'package:fipex_app/widgets/city_item.dart';
import 'package:fipex_app/widgets/custom_image.dart';
import 'package:fipex_app/widgets/feature_item.dart';
import 'package:fipex_app/widgets/guest_book.dart';
import 'package:fipex_app/widgets/notification_box.dart';
import 'package:fipex_app/widgets/category_item.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final _baseImg = "../../assets/icons";
  Repository repository = Repository();

  String _data = "";
  List<Guest> listGuest = [];

  getData() async {
    listGuest = await repository.getGuestdata();
  }

  scanBarcode() async {
    await FlutterBarcodeScanner.scanBarcode(
            "#000000", "Batal", true, ScanMode.QR)
        .then((value) => setState(() => _data = value));
  }

  @override
  void initState() {
    getData();
    repository.getToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: appBgColor,
      body: CustomScrollView(
        slivers: [
          // SliverAppBar(
          //   backgroundColor: appBarColor,
          //   pinned: true,
          //   snap: true,
          //   floating: true,
          //   title: getAppBar(),
          // ),
          SliverToBoxAdapter(
            child: buildBody(context),
          ),
        ],
      ),
      floatingActionButton: qrButton(),
    );
  }

  qrButton() {
    return InkWell(
      onTap: () => Navigator.popAndPushNamed(context, "/qrscanner"),
      child: Container(
        height: 75,
        width: 75,
        decoration:
            BoxDecoration(color: blue, borderRadius: BorderRadius.circular(50)),
        child: Icon(
          Icons.qr_code_scanner,
          size: 50,
          color: white,
        ),
      ),
    );
  }

  buildBody(context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(top: 5, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30.0),
                      child: CustomImage(
                        "../../assets/icons/logo_fipex.png",
                        width: 250,
                        height: 80,
                        radius: 10,
                        bgColor: appBgColor,
                        isShadow: false,
                        fit: BoxFit.fill,
                      ),
                    ),
                    // Text(repository.getToken().toString())
                  ],
                )
              ],
            ),
            getNews(),
            const SizedBox(
              height: 15,
            ),
            getCategory(),
            const Padding(
              padding: EdgeInsets.fromLTRB(15, 20, 15, 10),
              child: Text(
                "Guest Book",
                style: TextStyle(
                  color: textColor,
                  fontWeight: FontWeight.w500,
                  fontSize: 20,
                ),
              ),
            ),
            getGuestBook(size),
          ],
        ),
      ),
    );
  }

  getNews() {
    Size size = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
        aspectRatio: 1,
        height: 300,
        enlargeCenterPage: true,
        disableCenter: true,
        viewportFraction: 0.75,
      ),
      items: List.generate(
        3,
        (index) => FeatureItem(
          data: news[index],
          onTap: () {},
        ),
      ),
    );
  }

  getCategory() {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [
              Text(
                "Kategori Pameran",
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    color: textColor),
              ),
              // Text(
              //   "Lihat Semua",
              //   style: TextStyle(fontSize: 12, color: darker),
              // ),
            ],
          ),
        ),
        SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
          scrollDirection: Axis.horizontal,
          child: Row(
            children: List.generate(
              category.length,
              (index) => Padding(
                padding: const EdgeInsets.only(right: 10),
                child: CategoryItem(
                  data: category[index],
                  onTap: () {
                    setState(() {
                      selectedIndexGlobal.value = 1;
                    });
                    // if (selectedIndexGlobal.value != 1) return;
                  },
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  getGuestBook(Size size) {
    return Center(
      child: SizedBox(
        height: 300,
        width: size.width * 0.9,
        child: ListView.builder(
          shrinkWrap: true,
          itemCount: listGuest.length,
          itemBuilder: (context, index) {
            return SizedBox(
              width: size.width,
              height: 120,
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(children: [
                    CircleAvatar(
                      minRadius: 30,
                      maxRadius: 30,
                      backgroundImage: NetworkImage(
                          listGuest[index].img ?? skeletonImg[0]['img']),
                    ),
                    const SizedBox(
                      width: 24,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          listGuest[index].name,
                          style: TextStyles(context).getBoldStyle(),
                        ),
                        Expanded(
                          child: SizedBox(
                            height: 100,
                            width: size.width * 0.4,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Text(
                                listGuest[index].bio ?? "bio",
                                maxLines: 2,
                                style: TextStyles(context).getRegularStyle(),
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ]),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  getCities() {
    return SingleChildScrollView(
      padding: EdgeInsets.fromLTRB(15, 5, 0, 10),
      scrollDirection: Axis.horizontal,
      child: Row(
        children: List.generate(
          cities.length,
          (index) => Padding(
            padding: const EdgeInsets.only(right: 8),
            child: CityItem(
              data: cities[index],
              onTap: () {},
            ),
          ),
        ),
      ),
    );
  }
}
          // return ListTile(
          //   leading: CircleAvatar(
          //     child: Image.network(listGuest[index].img.toString()),
          //   ),
          //   title: Text(listGuest[index].name.toString()),
          //   trailing: Text(listGuest[index].bio.toString()),
          // );
          // FutureBuilder<List<dynamic>>(
          //   future: repository.fetchGuest(),
          //   builder: (context, snapshot) {
          //     if (snapshot.hasData) {
          //       return ListView.builder(
          //           padding: EdgeInsets.all(10),
          //           itemCount: snapshot.data!.length,
          //           itemBuilder: (context, index) {
          //             return ListTile(
          //               leading: CircleAvatar(
          //                 radius: 50,
          //                 backgroundImage: NetworkImage(
          //                     snapshot.data![index]['img_url'] ?? Container()),
          //               ),
          //               title: Text(snapshot.data![index]['name']),
          //               subtitle: Text(snapshot.data![index]['bio']),
          //             );
          //           });
          //     } else {
          //       return Center(
          //         child: CircularProgressIndicator(),
          //       );
          //     }
          //   },
          // ),

          // List.generate(
          //   recommends.length,
          //   (index) => Padding(
          //     padding: const EdgeInsets.only(right: 10),
          //     child: GuestBook(
          //       data: recommends[index],
          //       onTap: () {},
          //     ),
          //   ),
          // ),
        // ),


         // Widget getAppBar() {
  //   return Container(
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //       children: [
  //         Column(
  //           children: [
  //             Row(
  //               children: [
  //                 Icon(
  //                   Icons.place_outlined,
  //                   color: labelColor,
  //                   size: 20,
  //                 ),
  //                 SizedBox(
  //                   width: 3,
  //                 ),
  //                 Text(
  //                   "Phnom Penh",
  //                   style: TextStyle(
  //                     color: darker,
  //                     fontSize: 13,
  //                   ),
  //                 ),
  //               ],
  //             )
  //           ],
  //         ),
  //         Spacer(),
  //         NotificationBox(
  //           notifiedNumber: 1,
  //           onTap: () {},
  //         )
  //       ],
  //     ),
  //   );
  // }
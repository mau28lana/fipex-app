import 'package:badges/badges.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fipex_app/API/Model.dart';
import 'package:fipex_app/API/Repository.dart';
import 'package:fipex_app/widgets/main_button.dart';
import 'package:flutter/material.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/theme/text_style.dart';
import 'package:fipex_app/utils/data.dart';
import 'package:fipex_app/widgets/badges.dart';
import 'package:fipex_app/widgets/category_item.dart';
import 'package:fipex_app/widgets/feature_item.dart';
import 'package:fipex_app/widgets/guest_book.dart';
import 'package:fipex_app/widgets/product_poster.dart';
import 'package:http/http.dart';

class DetailProduct extends StatefulWidget {
  const DetailProduct({Key? key}) : super(key: key);

  @override
  State<DetailProduct> createState() => _DetailProductState();
}

class _DetailProductState extends State<DetailProduct> {
  Repository repository = Repository();
  Badges badges = Badges();

  String? productId;
  bool aku = false;

  int userBadge = 10;
  int silverBadge = 0;
  Products? productDetail;

  // getProductDetail() async {
  //   listProduct = await repository.getProductDetail(productId!);
  // }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final routesArgs = ModalRoute.of(context)?.settings.arguments as String;
    productId = routesArgs;
    repository
        .getProductDetail(productId: productId)
        .then((value) => setState((() => productDetail = value)));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: appBgColor,
        appBar: AppBar(),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: buildBody(size),
            )
          ],
        ));
  }

  buildBody(Size size) {
    return Padding(
        padding: EdgeInsets.only(top: 24.0),
        child: Column(
          children: [
            getPoster(),
            Container(
              margin: const EdgeInsets.symmetric(vertical: 18),
              width: size.width * 0.9,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(productDetail!.name.toString(),
                      style: TextStyles(context)
                          .getTitleStyle()
                          .copyWith(fontSize: 26)),
                ],
              ),
            ),
            getCreator(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [Text("123908")],
            ),
            getBadges(),
            getDescription(),
            getComment()
          ],
        ));
  }

  getBadges() {
    Size size = MediaQuery.of(context).size;
    return Column(
      children: [
        Badges(
          silverBadge: silverBadge,
          onTap: () {
            setState(() {
              if (silverBadge > 0) {
                silverBadge -= 1;
              }
              if (userBadge < 10) {
                userBadge += 1;
              }
            });
          },
        ),
        Badges(
          isProduct: false,
          title: "Lencana Anda",
          height: 140,
          onTap: () {
            if (userBadge > 0) {
              postComment(size);
              setState(() {
                userBadge -= 1;
                silverBadge += 1;
              });
            }
          },
          futureBadge: userBadge,
        ),
      ],
    );
  }

  getPoster() {
    Size size = MediaQuery.of(context).size;
    return CarouselSlider(
      options: CarouselOptions(
        aspectRatio: 0.1,
        height: 200,
        enlargeCenterPage: true,
        disableCenter: true,
        viewportFraction: .75,
      ),
      items: List.generate(
        features.length,
        (index) => ProductPoster(
          data: features[index],
        ),
      ),
    );
  }

  getCreator({onTap}) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      width: size.width * 0.9,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 12),
          Text(
            "Kreator",
            style: TextStyles(context).getBoldStyle().copyWith(fontSize: 20),
          ),
          SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(15, 5, 0, 5),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: List.generate(
                creator.length,
                (index) => Padding(
                    padding: const EdgeInsets.only(right: 10, top: 20),
                    child: getCreatorItem(data: creator[index])),
              ),
            ),
          ),
          const SizedBox(height: 12)
        ],
      ),
    );
  }

  getCreatorItem({onTap, required data}) {
    return GestureDetector(
      onTap: onTap,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              width: 75,
              height: 75,
              decoration: BoxDecoration(
                  color: labelColor, borderRadius: BorderRadius.circular(100)),
              child: data["icon"] ?? Icon(Icons.person)),
          const SizedBox(height: 24),
          Text(
            productDetail!.categoryId.toString(),
            style: TextStyles(context).getRegularStyle(),
          )
        ],
      ),
    );
  }

  getDescription() {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
        width: size.width * 0.9,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Deskripsi",
              style: TextStyles(context).getBoldStyle().copyWith(fontSize: 20),
            ),
            SizedBox(height: 8),
            Text(productDetail!.description.toString()),
          ],
        ));
  }

  getComment() {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 18),
      width: size.width * 0.9,
      child: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Komen",
              style: TextStyles(context).getBoldStyle().copyWith(fontSize: 20),
            ),
          ],
        ),
        Container(
            margin: const EdgeInsets.symmetric(vertical: 18),
            width: size.width * 0.9,
            height: 500,
            decoration: BoxDecoration(
                color: inActiveColor, borderRadius: BorderRadius.circular(8.0)),
            child: SingleChildScrollView(
              padding: const EdgeInsets.symmetric(vertical: 24),
              scrollDirection: Axis.vertical,
              child: Column(
                children: List.generate(
                  recommends.length,
                  (index) => Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: GuestBook(
                      data: recommends[index],
                      onTap: () {},
                    ),
                  ),
                ),
              ),
            )),
      ]),
    );
  }

  Future<dynamic> postComment(Size size) {
    return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        ),
        context: context,
        builder: (context) {
          return Container(
            padding: const EdgeInsets.all(18.0),
            height: size.height * 0.6,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                "Komen",
                style:
                    TextStyles(context).getBoldStyle().copyWith(fontSize: 20),
              ),
              const SizedBox(height: 24),
              TextField(
                minLines: 5,
                maxLines: 10,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: labelColor,
                    focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(color: labelColor),
                        borderRadius: BorderRadius.circular(8)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey.shade300),
                        borderRadius: BorderRadius.circular(8))),
              ),
              SizedBox(height: 24),
              MainButton(
                onTap: () {
                  Navigator.pop(context);
                },
                text: "Selesai",
                width: size.width * 0.84,
              )
            ]),
          );
        });
  }
}

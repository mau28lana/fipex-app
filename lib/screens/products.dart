import 'package:carousel_slider/carousel_slider.dart';
import 'package:fipex_app/API/Model.dart';
import 'package:fipex_app/API/Repository.dart';
import 'package:fipex_app/theme/text_style.dart';
import 'package:fipex_app/widgets/custom_image.dart';
import 'package:fipex_app/widgets/my_product.dart';
import 'package:fipex_app/widgets/product_poster.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/utils/data.dart';
import 'package:fipex_app/widgets/guest_book.dart';
import 'package:fipex_app/widgets/product_item.dart';
import 'package:fipex_app/widgets/category_item.dart';

class ProductsPage extends StatefulWidget {
  const ProductsPage({Key? key}) : super(key: key);

  @override
  State<ProductsPage> createState() => _ProductsPageState();
}

class _ProductsPageState extends State<ProductsPage>
    with TickerProviderStateMixin {
  Repository repository = Repository();
  Products products = Products();

  int selectedIndex = 0;

  List<Products> listProduct = [];
  List<Products> productsFiltered = [];
  List<Category> categories = [];

  getProductData() async {
    listProduct = await repository.getProducts();
    categories = await repository.getCategories();

    productsFiltered =
        listProduct.where((element) => element.id != "").toList();
  }

  @override
  void initState() {
    getProductData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    TabController _tabController = TabController(length: 2, vsync: this);
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        backgroundColor: appBgColor,
        body: CustomScrollView(slivers: [
          SliverAppBar(
            backgroundColor: appBarColor,
            pinned: true,
            snap: true,
            floating: true,
            centerTitle: true,
            title: CustomImage(
              "../../assets/icons/logo_fipex.png",
              isShadow: false,
              bgColor: appBarColor,
              radius: 5,
              width: 156,
              height: 50,
            ),
            bottom: TabBar(
                controller: _tabController,
                tabs: [Tab(text: "Semua Produk"), Tab(text: "Produk Saya")]),
          ),
          SliverToBoxAdapter(child: buildBody(_tabController, size.height))
        ]),
      ),
    );
  }

  getAppBar() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                  margin: const EdgeInsets.only(top: 24, bottom: 20),
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  width: double.infinity,
                  height: 40,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: searchContainer),
                  child: const TextField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(borderSide: BorderSide.none),
                        prefixIcon: Icon(
                          FontAwesomeIcons.magnifyingGlass,
                          size: 16,
                        ),
                        hintText: "Cari Produk...",
                        contentPadding: EdgeInsets.all(14)),
                  )),
            ],
          ),
        ),
      ],
    );
  }

  buildBody(controller, height) {
    return Container(
      width: double.maxFinite,
      height: height,
      child: TabBarView(
          controller: controller, children: [getProductContent(), MyProduct()]),
    );
  }

  // myProduct(Size size) {
  //   return
  // }

  getProductContent() {
    return Column(
      children: [
        getCategory(context),
        getProductCategory(),
      ],
    );
  }

  getProductCategory() {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(15, 20, 15, 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  categoryTitle(),
                  style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w700,
                      color: textColor),
                ),
              ],
            ),
          ),
          GridView.builder(
              shrinkWrap: true,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  mainAxisExtent: 220, childAspectRatio: 1, crossAxisCount: 2),
              itemCount: productsFiltered.length,
              itemBuilder: (_, index) {
                return ProductCard(
                  title: listProduct[index].name.toString(),
                  exhibition: "Fipex #4",
                  onTap: () {
                    showProductDetail(
                        context, listProduct[index].id.toString());
                  },
                );
              })
        ],
      ),
    );
  }

  getCategory(context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: 110,
      width: double.infinity,
      child: ListView.builder(
          shrinkWrap: true,
          itemCount: categories.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return SizedBox(
              width: size.width * 0.42,
              child: ListTile(
                minVerticalPadding: 24,
                title: Center(
                    child: CategoryItem(onTap: () {}, data: category[index])),
                tileColor: selectedIndex == index ? Colors.grey : null,
                onTap: () {
                  setState(() {
                    selectedIndex = index;
                    filteringProduct();
                  });
                },
              ),
            );
          }),
    );
  }

  filteringProduct() {
    if (selectedIndex == 0) {
      productsFiltered =
          listProduct.where((element) => element.name != "").toList();
    }
    if (selectedIndex == 1) {
      productsFiltered = listProduct
          .where((element) =>
              element.categoryId == categories[1].id ||
              element.name == "Desain Produk" ||
              element.name == "desain produk" ||
              element.name == "DESAIN PRODUK")
          .toList();
    }
    if (selectedIndex == 2) {
      productsFiltered = listProduct
          .where((element) =>
              element.categoryId == categories[2].id ||
              element.name == "Desain Web" ||
              element.name == "desain web" ||
              element.name == "DESAIN WEB")
          .toList();
    }
    if (selectedIndex == 3) {
      productsFiltered = listProduct
          .where((element) =>
              element.categoryId == categories[3].id ||
              element.name == "Game" ||
              element.name == "game" ||
              element.name == "GAME")
          .toList();
    }
    if (selectedIndex == 4) {
      productsFiltered = listProduct
          .where((element) =>
              element.categoryId == categories[4].id ||
              element.name == "Multimedia" ||
              element.name == "multimedia" ||
              element.name == "MULTIMEDIA")
          .toList();
    }
    if (selectedIndex == 5) {
      productsFiltered = listProduct
          .where((element) =>
              element.categoryId == categories[5].id ||
              element.name == "Pengembangan Perangkat Lunak" ||
              element.name == "pengembangan perangkat lunak" ||
              element.name == "PENGEMBANGAN PERANGKAT LUNAK" ||
              element.name == "PPL")
          .toList();
    }
  }

  categoryTitle() {
    if (selectedIndex == 0) {
      return "Semua Produk";
    }
    if (selectedIndex == 1) {
      return "Desain Produk";
    }
    if (selectedIndex == 2) {
      return "Desain Web";
    }
    if (selectedIndex == 3) {
      return "Game";
    }
    if (selectedIndex == 4) {
      return "Multimedia";
    }
    if (selectedIndex == 5) {
      return "Pengembangan Perangkat Lunak";
    }
  }

  showProductDetail(context, String productId) {
    Navigator.of(context).pushNamed('/detailproduct', arguments: productId);
  }
}

// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'dart:io';

import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:camera/camera.dart';
import 'package:fipex_app/screens/auth/Registration.dart';
import 'package:fipex_app/widgets/main_button.dart';
import 'package:fipex_app/widgets/main_form.dart';
import 'package:flutter/material.dart';
import 'package:fipex_app/API/Repository.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/widgets/custom_image.dart';
import 'package:image_picker/image_picker.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key? key}) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  Repository repository = Repository();
  final _formKey = GlobalKey<FormState>();
  final imagePicker = ImagePicker();

  bool isGuest = false;
  bool isRegister = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: buildBody(context),
    );
  }

  buildBody(context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: appBgColor,
      padding: EdgeInsets.all(12.0),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CustomImage(
              "../../assets/icons/logo_fipex.png",
              width: 200,
              height: 70,
              radius: 5,
              bgColor: appBgColor,
              isShadow: false,
              fit: BoxFit.fill,
            ),
            SizedBox(height: 42),
            Column(
              children: [
                Text(
                  "Buat akun",
                  style: TextStyle(fontSize: 34, fontWeight: FontWeight.w700),
                ),
                Container(
                  padding: EdgeInsets.only(top: 8.0),
                  height: size.height * 0.4,
                  width: size.width * 0.7,
                  child: Text(
                    "Dengan membuat akun, kamu bisa memberikan nilai ke produk favorit mu",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 24, color: Colors.grey.shade600),
                  ),
                ),
                MainButton(
                    text: "Login",
                    onTap: () {
                      Navigator.popAndPushNamed(context, "/auth/login");
                    }),
                MainButton(
                    text: "Register",
                    onTap: () {
                      Navigator.popAndPushNamed(context, "/auth/register");
                    }),
              ],
            )
          ],
        ),
      ),
    );
  }

  getButton(context, {onTap, text}) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: ElevatedButton(
        onPressed: onTap,
        style: ElevatedButton.styleFrom(primary: blue),
        child: SizedBox(
          height: 42,
          width: size.width * 0.7,
          child: Center(
            child: Text(
              text,
              style: TextStyle(
                  color: white, fontSize: 20, fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ),
    );
  }
}

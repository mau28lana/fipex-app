import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:email_validator/email_validator.dart';
import 'package:fipex_app/API/Repository.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/theme/text_style.dart';
import 'package:fipex_app/widgets/custom_image.dart';
import 'package:fipex_app/widgets/main_button.dart';
import 'package:fipex_app/widgets/main_form.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class RegistrationPage extends StatefulWidget {
  const RegistrationPage({Key? key}) : super(key: key);

  @override
  State<RegistrationPage> createState() => _RegistrationPageState();
}

class _RegistrationPageState extends State<RegistrationPage> {
  Repository repository = Repository();
  EmailValidator emailValidator = EmailValidator();

  final _formKey = GlobalKey<FormState>();

  final _nameController = TextEditingController();
  final _bioController = TextEditingController();
  final _emailController = TextEditingController();
  final _pwdController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: appBgColor,
      appBar: AppBar(
        title: getBack(),
        automaticallyImplyLeading: false,
        shadowColor: appBarColor,
        backgroundColor: appBgColor,
      ),
      body: buildBody(size),
    );
  }

  buildBody(Size size) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: size.height),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 24),
              CustomImage(
                "../../assets/icons/logo_fipex.png",
                width: 200,
                height: 70,
                radius: 5,
                bgColor: appBgColor,
                isShadow: false,
                fit: BoxFit.fill,
              ),
              const SizedBox(height: 24),
              Text("Registrasi",
                  style: TextStyles(context)
                      .getTitleStyle()
                      .copyWith(fontSize: 30)),
              const SizedBox(height: 24),
              getPict(),
              SizedBox(height: 24),
              Container(
                width: size.width * 0.8,
                margin: EdgeInsets.symmetric(vertical: 24),
                child: Form(
                  key: _formKey,
                  child: registration(),
                ),
              ),
              MainButton(
                  text: "Selanjutnya",
                  onTap: () async {
                    //validation temporary user
                    // if (widget.isGuest == true) {
                    //   if (_formKey.currentState!.validate()) {
                    //     await repository.loginData(
                    //         _emailController.text, _pwdController.text);
                    //   }
                    // }

                    //validation permanen user

                    if (_formKey.currentState!.validate()) {
                      await repository.registerData(
                          _nameController.text,
                          _bioController.text,
                          _emailController.text,
                          _pwdController.text);
                      Navigator.popAndPushNamed(context, "/");
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }

  getPict() {
    return Stack(
      children: [
        CustomImage(
          "../../assets/icons/person-icon.png",
          height: 120,
          width: 120,
          radius: 100,
        ),
        Positioned(
          right: 0,
          bottom: 0,
          child: ElevatedButton(
            onPressed: () {
              showAdaptiveActionSheet(
                context: context,
                title: const Text('Title'),
                androidBorderRadius: 30,
                actions: <BottomSheetAction>[
                  BottomSheetAction(
                      title: const Text('Ambil foto pakai kamera'),
                      onPressed: (context) {}),
                  BottomSheetAction(
                      title: const Text('Ambil gambar dari galeri'),
                      onPressed: (context) {}),
                ],
                cancelAction: CancelAction(title: const Text('Batal')),
              );
            },
            child: Icon(
              //<-- SEE HERE
              Icons.add_a_photo,
              color: white,
              size: 20,
            ),
            style: ElevatedButton.styleFrom(
              shape: CircleBorder(), //<-- SEE HERE
              padding: EdgeInsets.all(20),
            ),
          ),
        ),
      ],
    );
  }

  getLoginForm() {
    return Column(
      children: [
        getForm(
          isPerm: false,
          controller: _emailController,
          text: "Email",
        ),
        SizedBox(height: 24),
        getForm(
          isPerm: true,
          controller: _pwdController,
          text: "Password",
        ),
      ],
    );
  }

  registration() {
    return Column(
      children: [
        getForm(
            isPerm: false, text: "Nama Lengkap", controller: _nameController),
        SizedBox(height: 14),
        getForm(isPerm: false, text: "Bio", controller: _bioController),
        SizedBox(height: 14),
        getForm(
          isPerm: false,
          errMsg: "masukkan email yang valid",
          controller: _emailController,
          text: "Email",
        ),
        SizedBox(height: 14),
        getForm(
          isPerm: true,
          errMsg: "masukkan minimal 6 karakter",
          controller: _pwdController,
          text: "Password",
        ),
      ],
    );
  }

  getForm({isPerm, controller, text, errMsg}) {
    return TextFormField(
      obscureText: isPerm,
      controller: controller,
      decoration: InputDecoration(
          hintText: text,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0))),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "isian tidak boleh kosong";
        } else if (value != null && value.length < 6) {
          return errMsg ?? null;
        }
        if (value == _emailController) {
          if (value != null && !EmailValidator.validate(value)) {
            return errMsg ?? null;
          }
        }

        return null;
      },
    );
  }

  getBack() {
    return GestureDetector(
      child: Icon(
        Icons.arrow_back,
        color: mainColor,
      ),
      onTap: () {
        Navigator.popAndPushNamed(context, "/auth/landing");
      },
    );
  }
}

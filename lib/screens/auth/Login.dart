import 'package:adaptive_action_sheet/adaptive_action_sheet.dart';
import 'package:email_validator/email_validator.dart';
import 'package:fipex_app/API/Repository.dart';
import 'package:fipex_app/theme/color.dart';
import 'package:fipex_app/theme/text_style.dart';
import 'package:fipex_app/widgets/custom_image.dart';
import 'package:fipex_app/widgets/main_button.dart';
import 'package:fipex_app/widgets/main_form.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Repository repository = Repository();
  EmailValidator emailValidator = EmailValidator();

  final _formKey = GlobalKey<FormState>();
  final _emailController = TextEditingController();
  final _pwdController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: appBgColor,
      appBar: AppBar(
        title: getBack(),
        automaticallyImplyLeading: false,
        shadowColor: appBarColor,
        backgroundColor: appBgColor,
      ),
      body: buildBody(size),
    );
  }

  buildBody(Size size) {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: ConstrainedBox(
        constraints: BoxConstraints(minHeight: size.height),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 24),
              CustomImage(
                "../../assets/icons/logo_fipex.png",
                width: 200,
                height: 70,
                radius: 5,
                bgColor: appBgColor,
                isShadow: false,
                fit: BoxFit.fill,
              ),
              const SizedBox(height: 24),
              Text("Login",
                  style: TextStyles(context)
                      .getTitleStyle()
                      .copyWith(fontSize: 30)),
              const SizedBox(height: 24),
              SizedBox(height: 24),
              Container(
                width: size.width * 0.8,
                margin: EdgeInsets.symmetric(vertical: 24),
                child: Form(
                  key: _formKey,
                  child: getLoginForm(),
                ),
              ),
              MainButton(
                  text: "Selanjutnya",
                  onTap: () async {
                    if (_formKey.currentState!.validate()) {
                      await repository.reqLogin(
                          _emailController.text, _pwdController.text);

                      Navigator.pushNamed(context, "/");
                    }
                  })
            ],
          ),
        ),
      ),
    );
  }

  getLoginForm() {
    return Column(
      children: [
        getForm(
          isPwd: false,
          errMsg: "masukkan email yang valid",
          controller: _emailController,
          text: "Email",
        ),
        SizedBox(height: 14),
        getForm(
          isPwd: true,
          errMsg: "masukkan minimal 6 karakter",
          controller: _pwdController,
          text: "Password",
        ),
      ],
    );
  }

  getForm({isPwd, controller, text, errMsg}) {
    return TextFormField(
      obscureText: isPwd,
      controller: controller,
      decoration: InputDecoration(
          hintText: text,
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0))),
      validator: (value) {
        // if (value == null || value.isEmpty) {
        //   return "isian tidak boleh kosong";
        // } else if (value != null && value.length < 6) {
        //   return errMsg ?? null;
        // }
        // if (value == _emailController) {
        //   if (value != null && !EmailValidator.validate(value)) {
        //     return errMsg ?? null;
        //   }
        // }

        // return null;
      },
    );
  }

  getBack() {
    return GestureDetector(
      child: Icon(
        Icons.arrow_back,
        color: mainColor,
      ),
      onTap: () {
        Navigator.popAndPushNamed(context, "/auth/landing");
      },
    );
  }
}
